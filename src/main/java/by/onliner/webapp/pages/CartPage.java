package by.onliner.webapp.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.codeborne.selenide.Selenide.$;
import static org.assertj.core.api.Assertions.assertThat;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

@Log4j
public class CartPage {
    private WebDriver driver;

    public CartPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public SelenideElement numberOfProducts() {
        return $(By.xpath("//div[@class='cart-product']//input[@class='input-style input-style_primary input-style_small cart-product-add-box__input']"));

    }

    public SelenideElement plusButton() {
        return $(By.xpath("//div[@class='cart-product']//span[@class='button-style button-style_auxiliary button-style_small cart-product-add-box__button cart-product-add-box__button_add']"));
    }

    public SelenideElement singlePrice() {
        return $(By.xpath("//div[@class='cart-product__part cart-product__part_4']//span[contains(text(),'�.')]"));
    }

    public SelenideElement totalPrice() {
        return $(By.xpath("//div[@class='cart-navigation']//span[@data-bind='html: $root.format.positionPrice($root.positionsList.total().composite_price)']"));
    }

    public SelenideElement checkoutButton() {
        return $(By.xpath("//div[@class='cart-navigation']//a[contains(text(),'�������� ���� �����')]"));
    }

    public SelenideElement loginPopUp() {
        return $(By.xpath("//div[@class='popup-style__content']//div[contains(text(),'������� ��')]"));
    }

    public CartPage clickPlusButton() {
/*
        new WebDriverWait(driver, 10)
                .until(elementToBeClickable(numberOfProducts()));
        new WebDriverWait(driver, 10)
                .until(visibilityOfElementLocated(By.xpath("//div[@class='cart-product']//input[@class='input-style input-style_primary input-style_small cart-product-add-box__input']")));

 */

        $(numberOfProducts()).shouldBe(Condition.visible).click();
        $(plusButton()).shouldBe(Condition.visible).click();
        log.info("Click Plus Button");
        return this;
    }

    public CartPage verifyThatTotalPrice(){
        log.info("Verify That Total Price is correct");

        Double singlePriceToDouble;
        Double totalPriceToDouble;
        SelenideElement singlePrice = $(By.xpath("//div[@class='cart-product__part cart-product__part_4']//span[contains(text(),'�.')]"));;
        SelenideElement totalPrice = $(By.xpath("//div[@class='cart-navigation']//span[@data-bind='html: $root.format.positionPrice($root.positionsList.total().composite_price)']"));;

        singlePriceToDouble = Double.parseDouble(singlePrice.getText().replaceAll("[^,0-9]+", "").replaceAll(",", "."));
        log.info("singlePriceToDouble = " + singlePriceToDouble);

        totalPriceToDouble = Double.parseDouble(totalPrice.getText().replaceAll("[^,0-9]+", "").replaceAll(",", "."));
        log.info("singlePriceToDouble = " + totalPriceToDouble);

        $(totalPrice).isDisplayed();

        assertThat(totalPriceToDouble).as("Total price is incorrect!").isEqualTo(singlePriceToDouble * 2);
        log.info("Passed");

        return this;
    }

    public CartPage doCheckout() throws InterruptedException {
        $(checkoutButton()).click();
        log.info("Do Checkout");
        return this;
    }

    public CartPage verifyThatLoginPopup() throws InterruptedException {
        log.info("Verify That Login Popup appears");

        /*
        new WebDriverWait(driver, 10)
                .until(visibilityOfElementLocated(By.xpath("//div[@class='popup-style__content']//div[contains(text(),'������� ��')]")));

         */

        $(By.xpath("//div[@class='popup-style__content']//div[contains(text(),'������� ��')]")).isDisplayed();
 //       assertThat(loginPopUp().isDisplayed()).as("No Login Pop Up!").isTrue();
        log.info("Passed");

        return this;
    }

}

