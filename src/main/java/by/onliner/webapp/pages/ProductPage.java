package by.onliner.webapp.pages;

import com.codeborne.selenide.SelenideElement;
import lombok.extern.log4j.Log4j;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.shouldHaveThrown;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

@Log4j
public class ProductPage {
    private WebDriver driver;

    public ProductPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public SelenideElement addToCartButton() {
        return $(By.xpath("//aside[@class='product-aside js-product-aside']//a[@class='button button_orange product-aside__item-button']"));
    }


    public SelenideElement cartIconHeader() {
        return $(By.xpath("//div[@class='b-top-actions']//a[@title='�������']"));
    }


    public SelenideElement numberOfProductsInCartHeader(int numberOfProductsInCart) {
        return $(By.xpath(String.format("//div[@class='b-top-actions']//div[contains(text(),'%d') and contains(@class,'auth-bar__counter')]", numberOfProductsInCart)));
    }

    public SelenideElement typeOfProduct(String typeOfProduct) {
        return $(By.xpath(String.format("//div[@class='product-specs__main-group product-specs__group--full js-specs-full is-visible']//span[contains(text(),'%s')]", typeOfProduct)));
    }





    public ProductPage addProductToCart() throws InterruptedException {
/*
        new WebDriverWait(driver, 10)
                .until(visibilityOfElementLocated(By.xpath("//aside[@class='product-aside js-product-aside']//a[@class='button button_orange product-aside__item-button']")));

 */
 
        $(addToCartButton()).shouldBe(visible).click();

        log.info("Add Product To Cart");

        return this;
    }

    public CartPage openCartHeader() throws InterruptedException {
/*
        new WebDriverWait(driver, 10)
                .until(visibilityOfElementLocated(By.xpath("//div[@class='b-top-actions']//a[@title='�������']")));

 */
  
        $(cartIconHeader()).click();
        log.info("Open Cart in Header");

        return new CartPage(driver);
    }

    public ProductPage verifyNumberOfProductsInCartHeader(int numberOfProductsInCart) throws InterruptedException {

        log.info("Verify Number Of Products In Cart Header");
 /*
        new WebDriverWait(driver, 10)
                .until(visibilityOfElementLocated(By.xpath("//div[@class='b-top-actions']//div[contains(@class,'auth-bar__counter')]")));

  */
//        assertThat(numberOfProductsInCartHeader(numberOfProductsInCart).isDisplayed()).as("Number Of Products In Cart Header is incorrect!").isTrue();
        $(numberOfProductsInCartHeader(numberOfProductsInCart)).shouldBe(visible);
        $(numberOfProductsInCartHeader(numberOfProductsInCart)).isDisplayed();
        $(numberOfProductsInCartHeader(numberOfProductsInCart)).shouldHave(text(String.valueOf(numberOfProductsInCart)));
        System.out.println("!!! numberOfProductsInCartHeader :" + numberOfProductsInCartHeader(numberOfProductsInCart).getText());

        log.info("Passed");

        return this;
    }

    public ProductPage verifyThatProductDescription(String typeOfProduct) {

/*
        new WebDriverWait(driver, 10)
                .until(visibilityOfElementLocated(By.xpath("//div[@class='product-specs__main-group product-specs__group--full js-specs-full is-visible']//span[contains(text(),'������������')]")));
*/
        SoftAssertions softly = new SoftAssertions();
        log.info("Verify That Product Description contains: " + typeOfProduct);
//        softly.assertThat(typeOfProduct(typeOfProduct).isDisplayed()).as("Product description is incorrect!").isTrue();
        $(typeOfProduct(typeOfProduct)).isDisplayed();

        log.info("Passed");


     //   $$(By.xpath("//div[@class='product-specs__main-group product-specs__group--full js-specs-full is-visible']//span[@class='value__text']")).get(0).shouldHave();
     //   $(By.xpath("//div[@class='product-specs__main-group product-specs__group--full js-specs-full is-visible']//span[@class='value__text']").get(1);

        //       softly.assertAll();

        return this;
    }




}