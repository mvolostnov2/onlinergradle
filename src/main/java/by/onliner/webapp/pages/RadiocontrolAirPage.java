package by.onliner.webapp.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.google.common.collect.Comparators;
import com.google.common.collect.Ordering;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.assertj.core.api.Assertions.assertThat;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public class RadiocontrolAirPage {
    private static final Logger log = Logger.getLogger(RadiocontrolAirPage.class);

    private WebDriver driver;
    JavascriptExecutor js = (JavascriptExecutor) driver;

    public RadiocontrolAirPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public SelenideElement leftMenuItem(String leftMenuItem) {
        return $(By.xpath(String.format("//div[@id='schema-filter']//span[text()='%s']", leftMenuItem)));
    }

    public SelenideElement sortingMenu() {
        return $(By.xpath(String.format("//div[@class='js-schema-results schema-grid__center-column']//a[@class='schema-order__link']")));
    }

    public SelenideElement sortingMenuItem(String sortingMenuItem) {
        return $(By.xpath(String.format("//div[@class='schema-order__popover']//span[text()='%s']"
                , sortingMenuItem)));
    }


    public SelenideElement compareButtonWithProducts(int numberOfProducts) {
        return $(By.xpath(String.format("//div[@class='compare-button compare-button_visible']//span[contains(text(),'%d')]"
                ,numberOfProducts)));
    }

    public SelenideElement compareButton() {
        return $(By.xpath(String.format("//div[@class='compare-button compare-button_visible']//span[contains(text(),'������')]"
        )));
    }

    public SelenideElement numberOfSearchResults(int numberOfSearchResults) {
        return $(By.xpath(String.format("//div[@class='schema-filter-button__state schema-filter-button__state_initial schema-filter-button__state_disabled schema-filter-button__state_control']//span[contains(text(),'������� %d')]"
                ,numberOfSearchResults )));

    }

    public RadiocontrolAirPage scrollToLeftMenuElement(String leftMenuElement) {

        $(By.xpath(String.format("//div[@id='schema-filter']//span[text()='%s']", leftMenuElement))).scrollTo();
//        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath(String.format("//div[@id='schema-filter']//span[text()='%s']", leftMenuElement))));
        log.info("Scroll to Left Menu Element: " + leftMenuElement);
        return new RadiocontrolAirPage(driver);
    }

    public RadiocontrolAirPage scrollToPageHeader() {

        $((By.xpath("//h1[@class='schema-header__title']"))).scrollTo();
//        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
//                driver.findElement(By.xpath("//h1[@class='schema-header__title']")));
        log.info("Scroll to Page Header" );
        return this;
    }

    public RadiocontrolAirPage selectLeftMenuItem(String leftMenuItem)  {
//        new WebDriverWait(driver, 10)
//                .until(elementToBeClickable(leftMenuItem(leftMenuItem)));
        $(leftMenuItem(leftMenuItem)).click();
        log.info("Select Left Menu Item: " + leftMenuItem);
        return new RadiocontrolAirPage(driver);
    }

    public RadiocontrolAirPage selectSortingBy(String sortingMenuItem) throws InterruptedException {

        //new WebDriverWait(driver, 10).until(elementToBeClickable(sortingMenu()));
        $(sortingMenu()).click();
        log.info("Open Sorting menu");
//        new WebDriverWait(driver, 10).ignoring(ElementClickInterceptedException.class)
 //               .until(elementToBeClickable(By.xpath("//div[@class='js-schema-results schema-grid__center-column']//a[@class='schema-order__link']")));

        $(sortingMenuItem(sortingMenuItem)).click();
        log.info("Select Sorting by: " + sortingMenuItem);

        return new RadiocontrolAirPage(driver);
    }

    public RadiocontrolAirPage setMinimalRange(String minimalRage)  {
        $(By.xpath("//div[@id='schema-filter']//input[@placeholder=5]")).sendKeys(minimalRage);
        log.info("Set Minimal Range: " + minimalRage);
        return new RadiocontrolAirPage(driver);
    }

    public RadiocontrolAirPage openAdditionalParameters()  {
        $(By.xpath("//div[@id='schema-filter']//a[@data-bind='click: $root.toggleAdditionalParameters.bind($root)']")).click();
        log.info("Open Additional Parameters");
        return new RadiocontrolAirPage(driver);
    }

    public RadiocontrolAirPage selectProductByIndex(int index) {
//        new WebDriverWait(driver, 10).ignoring(ElementClickInterceptedException.class)
//                .until(visibilityOfElementLocated(By.xpath("//div[@id='schema-products']//span[@class='i-checkbox__faux']")));
 //       List<WebElement> selectedProducts = driver.findElements(By.xpath("//div[@id='schema-products']//span[@class='i-checkbox__faux']"));
        ElementsCollection   selectedProducts =     $$(By.xpath("//div[@id='schema-products']//span[@class='i-checkbox__faux']"));
        selectedProducts.get(index).click();
        log.info("Select product # " + index);
        return this;
    }

    public ComparisonPage openComparison()  {
//        new WebDriverWait(driver, 10)
//                .until(visibilityOfElementLocated(By.xpath("//div[@class='compare-button compare-button_visible']//span[contains(text(),'������')]")));
        $(compareButton()).click();
        log.info("Open Comparison");
        return new ComparisonPage(driver);
    }



    public RadiocontrolAirPage checkThatPageHeaderContains(String headerName) {
        String pageHeader = $(By.xpath("//h1[@class='schema-header__title']")).getText();
        log.info("Checking Page Header");
        if (pageHeader.contains(headerName)) {
            log.info("Page header name is correct: " + pageHeader);
        } else {
            log.info("Warning! Page header name is incorrect: " + pageHeader + " instead of " + headerName);
        }
//        assertThat(pageHeader).as("Incorrect Page header", pageHeader).contains(headerName);
        return this;
    }

    public RadiocontrolAirPage verifyThatNumberOfSearchResultsEqualsTo(int numberOfSearchResults) {
        log.info("Verification that Number of Search Results is correct");
//        new WebDriverWait(driver, 10)
//                .until(visibilityOfElementLocated(By.xpath(String.format("//div[@class='schema-filter-button__state schema-filter-button__state_initial schema-filter-button__state_disabled schema-filter-button__state_control']//span[contains(text(),'������� %d')]"
//                        , numberOfSearchResults ))));



        log.info("numberOfSearchResults: " + numberOfSearchResults);
        $(numberOfSearchResults(numberOfSearchResults)).isDisplayed();
        $(numberOfSearchResults(numberOfSearchResults)).shouldHave(text(String.valueOf(numberOfSearchResults)));
 //       assertThat(numberOfSearchResults(numberOfSearchResults).isDisplayed()).as("�������: " + numberOfSearchResults).isTrue();
        return this;
    }



    public RadiocontrolAirPage verifyThatSortingIsCorrect() throws InterruptedException {
        log.info("Verify that Sorting is correct");
/*
        new WebDriverWait(driver, 10).ignoring(StaleElementReferenceException.class)
                .until(visibilityOfElementLocated(By.xpath("//span[text()='42,00��.']")));

 */
/*
        new WebDriverWait(driver, 10)
                .until(visibilityOfElementLocated(By.xpath("//div[@class='schema-product__part schema-product__part_2']//span[contains(text(),'�.')]")));

 */
        //       driver.manage().timeouts().pageLoadTimeout(2000,TimeUnit.MILLISECONDS);
        //       driver.manage().timeouts().implicitlyWait(2000,TimeUnit.MILLISECONDS);
//        driver.manage().timeouts().wait(2000);


        Thread.sleep(2000);
/*
        new WebDriverWait(driver, 10)
                .until(visibilityOfElementLocated(By.xpath("//div[@class='schema-product__part schema-product__part_2']//span[contains(text(),'�.')]")));

 */

        $(By.xpath("//div[@class='schema-product__part schema-product__part_2']//span[contains(text(),'�.')]")).shouldBe(visible);

        log.info("Product price is visible");


        List<Double> priceToDouble = new ArrayList<Double>();
//        List<WebElement> price = driver.findElements(By.xpath("//div[@class='schema-product__part schema-product__part_2']//span[contains(text(),'�.')]"));
        ElementsCollection price = $$(By.xpath("//div[@class='schema-product__part schema-product__part_2']//span[contains(text(),'�.')]"));

        log.info("Collection of prices is formed");

        /*
        for (int i =0; i<= price.size(); i++) {
 //           System.out.println(element[i].getText());
            System.out.println(price.get(i).getText());
        }

         */


     double qwe = Double.parseDouble(price.get(0).getText().replaceAll("[^,0-9]+", "").replaceAll(",", "."));

        System.out.println("!!!! " + qwe);


        for (SelenideElement element : price) {
            priceToDouble.add(Double.parseDouble(element.getText().replaceAll("[^,0-9]+", "").replaceAll(",", ".")));
        }

        log.info("Collection of prices is converted to double ");

/*
        boolean isSorted = Ordering.natural().isOrdered(priceToDouble);
        System.out.println(isSorted);

 */
/*
        boolean isSorted = Comparators.isInOrder(priceToDouble, Comparator.<Double> naturalOrder());
        System.out.println("Whyyyyy??????? " + isSorted);

 */


/*
        String verifySortingOrder = (priceToDouble.get(1) >= priceToDouble.get(0)) ? "Sorting is correct!" : "Sorting is incorrect!";
        System.out.println(verifySortingOrder);

 */

        assertThat(priceToDouble.get(1) >= priceToDouble.get(0)).as("Sorting is incorrect! "  + priceToDouble.get(1) + " is not >= " + priceToDouble.get(0)).isTrue();
        log.info("Sorting is Correct: " + priceToDouble.get(1) + " > " + priceToDouble.get(0));

        return this;
    }

/*

    public static boolean sortingIsCorrect (List<Double> priceToDouble) {

        log.info("!!! sortingIsCorrect !!!");

        return Comparators.isInOrder(priceToDouble, Comparator.<Double> naturalOrder());

    }
*/


    public RadiocontrolAirPage verifyThatCompareButtonContainsNumberOfSelectedProducts(int numberOfProducts) throws InterruptedException {
        log.info("Verify That Compare Button contains " + numberOfProducts + " products");
/*
        new WebDriverWait(driver, 10)
                .until(elementToBeClickable(By.xpath("//div[@class='compare-button compare-button_visible']//span[contains(text(),'4')]")));

 */

        $(compareButtonWithProducts(numberOfProducts)).isDisplayed();
        $(compareButtonWithProducts(numberOfProducts)).shouldHave(text(String.valueOf(numberOfProducts)));


        //     Assert.assertTrue(compareButtonWithProducts(numberOfProducts).isDisplayed(), "Cant find Compare Button with " + numberOfProducts + " products");
  //      assertThat(compareButtonWithProducts(numberOfProducts).isDisplayed()).as("Cant find Compare Button with " + numberOfProducts + " products").isTrue();
        log.info("Passed");
        return this;
    }

}
