package by.onliner.test;


import by.onliner.webapp.WebApplication;
import com.codeborne.selenide.Configuration;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.extern.log4j.Log4j;
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.BrowserType;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;


@Log4j
public class BaseTest {




    public static WebDriver driver;
    private static final String HOMEPAGE_URL = "https://onliner.by/";
    protected static WebApplication app = new WebApplication();


    @BeforeSuite(alwaysRun = true)
    public void browserSetup() {

        driver = initDriver(BrowserType.CHROME);
        log.info("Open browser " + driver);

//        Configuration.startMaximized = true;
        Configuration.browserSize = "1920x1080";


 //       driver.manage().window().maximize();
//        log.info("Maximize browser's window");
        log.info("Set browserSize = 1920x1080");


    }

    @BeforeMethod
    public void openPage() {
        open(HOMEPAGE_URL);
 //               driver.get(HOMEPAGE_URL);
        log.info("Open https://onliner.by/");

    }


    @AfterSuite(alwaysRun = true)
    public void browserTearDown() {
        log.info("Close browser" + driver);
        driver.quit();
        driver=null;
    }


    private WebDriver initDriver(String browserType) {
        if (browserType.equals(BrowserType.CHROME)) {
            WebDriverManager.chromedriver().setup();
            return new ChromeDriver();
        }
        if (browserType.equals(BrowserType.FIREFOX)) {
            WebDriverManager.firefoxdriver().setup();
            return new FirefoxDriver();
        }
        if (browserType.equals(BrowserType.EDGE)) {
            WebDriverManager.edgedriver().setup();
            return new EdgeDriver();
        }
        return null;
    }
}
