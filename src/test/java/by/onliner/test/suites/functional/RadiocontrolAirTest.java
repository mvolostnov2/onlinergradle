package by.onliner.test.suites.functional;

import by.onliner.test.BaseTest;
import lombok.extern.log4j.Log4j;
import org.testng.annotations.Test;

@Log4j
public  class RadiocontrolAirTest extends BaseTest {

    @Test(description = "Header ���������������� ���������� test")
    public void headerIsCorrect() throws InterruptedException {
/*
        log.info("Startring test: Header ���������������� ���������� test");
        log.debug("Startring test: Header ���������������� ���������� test");
        log.warn("Startring test: Header ���������������� ���������� test");
        log.error("Startring test: Header ���������������� ���������� test");
        log.fatal("Startring test: Header ���������������� ���������� test");

 */

        app.openHomePage()
                .openTopMenuSection("�������")
                .selectCatalogCategory("������� ������")
                .selectCatalogSubCategory("�����")
                .selectProductType("���������������� ����������")
                .checkThatPageHeaderContains("���������������� ����������")
                .scrollToLeftMenuElement("���")
                .selectLeftMenuItem("������������")
                .selectLeftMenuItem("�������")
                .selectLeftMenuItem("������")
                .setMinimalRange("100")
                .scrollToLeftMenuElement("��������� ��������, �")
                .openAdditionalParameters()
                .selectLeftMenuItem("���������������")
                .verifyThatNumberOfSearchResultsEqualsTo(102)
                .scrollToPageHeader()
                .selectSortingBy("�������")
                .verifyThatSortingIsCorrect()
                .selectProductByIndex(0)
                .selectProductByIndex(2)
                .selectProductByIndex(4)
                .selectProductByIndex(5)
                .verifyThatCompareButtonContainsNumberOfSelectedProducts(4)
                .openComparison()
                .selectComparedProductWithIndex(2)
                .verifyThatProductDescription("������������")
                .verifyThatProductDescription("�������")
//                .verifyThatProductDescription("������")
                .verifyThatProductDescription("���������������")
                .addProductToCart()
                .verifyNumberOfProductsInCartHeader(1)
                .openCartHeader()
                .clickPlusButton()
                .verifyThatTotalPrice()
                .doCheckout()
                .verifyThatLoginPopup()

        ;

    }

}
